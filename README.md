# PaintPy

## Description
[Description du projet]

## Pour commencer
Ces instructions vous permettront d'obtenir une copie du projet sur votre ordinateur.

### Prérequis
Les logiciels à installer sont :
* PyGame
```console
foo@bar:~$ sudo apt-get install python3-pygame
```

## Remerciements
* **[Rausy Pablo]** - [Site internet](https://pablo.rauzy.name/)

## Auteurs
* **JETON Alex** - *Initial work* - [GitLab](https://gitlab.com/Spaar)

## License
Ce projet est sous licence MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails.